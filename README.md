# Final DeCharla

Repositorio de plantilla para el proyecto final del curso 2022-2023.

Para entregar el proyecto depositado en este repositorio, leer con atención el apartado sobre la entrega del enunciado del ejercicio. Antes de entregar, rellenar el texto que viene a continuación según indica dicho enunciado, y borrar toda esta primera parte.

<<<<<BORRAR HASTA AQUí, INCLIUDA ESTA LÍNEA>>>>>

# ENTREGA CONVOCATORIA XXX

# ENTREGA DE PRÁCTICA

## Datos

* Nombre:
* Titulación:
* Cuenta en laboratorios:
* Cuenta URJC:
* Video básico (url):
* Video parte opcional (url):
* Despliegue (url):
* Contraseñas:
* Cuenta Admin Site: usuario/contraseña

## Resumen parte obligatoria

## Lista partes opcionales

* Nombre parte:
* Nombre parte:
* ...
